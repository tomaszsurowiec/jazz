package servlets;
import java.io.IOException;


import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kredyts.Stalykredyt;
import kredyts.malejacykredyt;


@WebServlet("/results")
public class ResultsServlet extends HttpServlet{
	
		/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
		/**
	 * 
	 */
		public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException
		{
			response.sendRedirect("/");
		}
		public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException
		{
			String kredyt = request.getParameter("kredyt");
			String kapital = request.getParameter("kapital");
			String NumerRaty = request.getParameter("NumerRaty");
			String Procent = request.getParameter("Procent");
			
			double doublekapital = Double.parseDouble(kapital);
			double doubleNumerRaty = Double.parseDouble(NumerRaty);
			double doubleProcent = Double.parseDouble(Procent);
			
			response.setContentType("text/html");
			String Typkredyt;
			if (kredyt.equals("malejacy"))
				Typkredyt="kredyt malej�cy";
			else
				Typkredyt="kredyt sta�y";
			
			response.getWriter().println("<h1>Wybrales kredyt:"
					+Typkredyt
					+".</h1>");
			
			if (kredyt.equals("malejacy"))
			{
				malejacykredyt kredyt2=new malejacykredyt(doublekapital,
						doubleNumerRaty,doubleProcent);
				double pomoc,pomoc2;
				int i=1;
				response.getWriter().println("<table><tr><td><b>Po�yczasz(kapita�)"
						+ "</b></td><td>"
						+String.format( "%.2f", doublekapital )
						+"</td></tr><tr><td><b>Procent"
						+ "</b></td><td>"
						+String.format( "%.2f", doubleProcent )
						+"</td></tr><tr><td><b>Ilo�� rat"
						+ "</b></td><td>"
						+String.format( "%.2f", doubleNumerRaty )
						+ "</td></tr></table><table>");
				response.getWriter().println("<tr><td><b>Nr raty</b></td>"
						+ "<td><b>Rata kapita�owa</b></td>"
						+ "<td><b>Rata odsetkowa</b></td>"
						+ "<td><b>Ca�kowita rata</b></td></tr>");
				while (i<=doubleNumerRaty)
				{
					pomoc=kredyt2.getkapitalInstalment();
					pomoc2=kredyt2.calculateInstalment(i);
					response.getWriter().println("<tr><td>"
						+i
						+"</td><td>"
						+String.format( "%.2f", pomoc )
						+" z�.</td><td>"
						+String.format( "%.2f", pomoc2 )
						+" z�.</td><td>"
						+String.format( "%.2f", pomoc+pomoc2 )
						+" z�.</td><td></tr>");
					i++;
				}
				pomoc=kredyt2.getSum();
				response.getWriter().println("</table>"
						+ "<table><tr><td> W sumie do sp�aty</td><td> "
						+String.format( "%.2f", pomoc )
						+"</td></tr></table>");
			}
			else
			{
			
				Stalykredyt kredyt1=new Stalykredyt(doublekapital,
									doubleNumerRaty,doubleProcent);
				double pomoc;
				int i=1;
				response.getWriter().println("<table><tr><td><b>Po�yczono(kapita�)"
					+ "</b></td><td>"
					+String.format( "%.2f", doublekapital )
					+"</td></tr><tr><td><b>Procent"
					+ "</b></td><td>"
					+String.format( "%.2f", doubleProcent )
					+"</td></tr><tr><td><b>Ilo�� rat"
					+ "</b></td><td>"
					+String.format( "%.2f", doubleNumerRaty )
					+ "</td></tr></table><table>");
				response.getWriter().println("<tr><td><b>Nr raty</b></td>"
					+ "<td><b>Rata</b></td></tr>");
				while (i<=doubleNumerRaty)
				{
					pomoc=kredyt1.getInstalment();
					response.getWriter().println("<tr><td>"
						+i
						+"</td><td>"
						+String.format( "%.2f", pomoc )
						+" z�.</td></tr>");
					i++;
				}
				pomoc=kredyt1.getSum();
				response.getWriter().println("</table>"
					+ "<table><tr><td> W sumie do sp�aty</td><td> "
					+String.format( "%.2f", pomoc )
					+"</td></tr></table>");
			}
			
		}

}

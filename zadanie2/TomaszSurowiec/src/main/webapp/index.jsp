<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Kalkulator kredytowy</title>
	</head>
	<body>
		<h1>Witamy w banku</h1>
		<%-- Aby bylo metoda get: <form action="hello" method="get"> --%>
		<form method ="post" action="results">
			<div>
				<p>
					Jaki rodzaj kredytu?: &nbsp;&nbsp;&nbsp;
					<select name="credit" size=1>
						<option value="diminishing">
							Kredyt malejący lub Rata malejąca
						</option>
						<option value="constant">
							Kredyt stały lub Rata stała
						</option>
					</select> 
				</p>
				<p>
					Ile chcesz pozyczyc od nas pieniedzy:&nbsp;&nbsp;&nbsp;
					<input type="number" name="capital" min="100" required />
				</p>
				<p>
					Na ile miesicy chcesz wziac kredyt:&nbsp;&nbsp;&nbsp;
					<input type="number" name="numberOfInstalments" min="6" required />
				</p>
				<p>	
					jakie opocentowanie?:&nbsp;&nbsp;&nbsp;
					<input type="number" name="percent" step="any" min="0.1" required />
				</p>
				<br/>
				<input type="submit" value="Oblicz"/>
 
			</div>

		</form>

	</body>
</html>
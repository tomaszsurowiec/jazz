package credits;

import static java.lang.Math.*;

public class DiminishingCredit {
		
		private double KapitalNaRaty;
		private double LiczbaRat;
		private double procent;
		private double Kapital;
		private double ProcenRaty;
		private double suma;
		
		
		public DiminishingCredit(double Kapital, double LiczbaRat,
							  double procent)
		{
			this.Kapital=Kapital;
			this.LiczbaRat=LiczbaRat;
			this.procent=procent;
			this.KapitalNaRaty=this.Kapital/this.LiczbaRat;
			this.ProcenRaty=this.Kapital*this.procent/(12.0*100.0);
		}
		
		public double ObliczRate(int m)
		{
			double Rata;
			Rata=((this.Kapital-((m-1)*this.KapitalNaRaty))*
					this.procent)/(12.0*100.0);
			return Rata;
		}
		
		private void ObliczRaty()
		{
			this.KapitalNaRaty=this.Kapital/this.LiczbaRat;
			this.ProcenRaty=this.Kapital*this.procent/(12.0*100.0);
		}
		
		private void suma()
		{
			double suma;
			suma=this.Kapital;
			int i=1;
			while (i<=this.LiczbaRat)
			{
				suma=suma+this.ObliczRate(i);
				i++;
			}
			this.suma=suma;
		}
		public double WezProcenRaty() {
			this.ObliczRate();
			return ProcenRaty;
		}
		
		public double WezKapitalNaRaty() {
			this.ObliczRate();
			return KapitalNaRaty;
		}
		public double WezSume() {
			this.CalculateSum();
			return suma;
		}
		public void ustawLiczbaRat(double LiczbaRat) {
			this.LiczbaRat = LiczbaRat;
		}
		public double WezKapital() {
			return Kapital;
		}
		public void UstawProcent(double procent) {
			this.procent = procent;
		}
		
		public void UstawKapital(double Kapital) {
			this.Kapital = Kapital;
		}
		public double WezLiczbaRat() {
			return LiczbaRat;
		}
		
		public double WezProcent() {
			return procent;
		}
		
}
